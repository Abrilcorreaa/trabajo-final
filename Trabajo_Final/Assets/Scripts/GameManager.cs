using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public TMP_Text PiezasRec;
    public int Piezas;
    
    public void addnum()
    {
        Piezas++;
        PiezasRec.text = Piezas.ToString();
        
    }
}
