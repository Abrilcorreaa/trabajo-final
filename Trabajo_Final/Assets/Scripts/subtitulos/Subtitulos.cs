using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class Subtitulos : MonoBehaviour
{
    [SerializeField]TextMeshProUGUI textSubtitle = default;

    public static Subtitulos instance;
    
    public void Awake()
    {
        instance = this;
        LimpiarPant();
        
    }

    public void PonerSubtitulos(string sub, float delay)
    {
        textSubtitle.text = sub;
        StartCoroutine(ClearAfterSec(delay));
        
    }
   
    public void LimpiarPant()
    {
        textSubtitle.text = "";
        
    }
    private IEnumerator ClearAfterSec(float delay)
    {
        yield return new WaitForSeconds(delay); 
        LimpiarPant();
    }
    
}
