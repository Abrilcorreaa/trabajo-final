using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music2 : MonoBehaviour
{
    public Transform objeto2;
   
    public string nombreMusica = "musica_cercania1";
    private AudioManager audioManager;

    void Start()
    {
         
        audioManager = AudioManager.instancia;
        audioManager.ReproducirSonido(nombreMusica);
    }

    void Update()
    {
        if (objeto2 != null )
        {
            
            float distancia2 = Vector3.Distance(transform.position, objeto2.position);
            float distanciaMaxima = 22f;

            float factorVolumen2 = Mathf.Clamp01(1 - (distancia2 / distanciaMaxima));

            audioManager.sonidos[1].FuenteAudio.volume = factorVolumen2;

        }
        
    }
}
