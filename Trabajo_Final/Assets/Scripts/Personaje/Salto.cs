using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    private Rigidbody rb;
    private CapsuleCollider col;
    public float fuerza = 20;
    public float magnitudSalto = 10;
    public LayerMask capaPiso;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (Toca_el_Piso()))
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);

        }

    }
    private bool Toca_el_Piso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
}
