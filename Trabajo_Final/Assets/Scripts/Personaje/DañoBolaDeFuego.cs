using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoBolaDeFuego : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BolaDeFuego"))
        {
            GameManagerPersonaje gameManager = FindObjectOfType<GameManagerPersonaje>();

            gameManager.addnum();
        }

    }
}
