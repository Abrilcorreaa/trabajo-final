using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    
    public GameObject Ubicacion_bala;
    
    public GameObject BalaPrefab;
    
    public float Velocidad_bala;

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameObject Bala2 = Instantiate(BalaPrefab, Ubicacion_bala.transform.position, Ubicacion_bala.transform.rotation) as GameObject;

            Rigidbody rb = Bala2.GetComponent<Rigidbody>();

            rb.AddForce(transform.forward * Velocidad_bala);

            Destroy(Bala2, 5.0f);
        }
    }
}
