using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class estamina : MonoBehaviour
{
    public float Estamina;
    float MaxEstamina;

    public Slider EstaminaSlider;
    public float valor;

    void Start()
    {
        MaxEstamina = Estamina;
        EstaminaSlider.maxValue = MaxEstamina;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            BajarEnergia();
        }
        else if (Estamina != MaxEstamina)
        {
            SubirEnergia();
        }

        EstaminaSlider.value = Estamina;
    }

    public void BajarEnergia()
    {
        if (Estamina > 0)
        {
            Estamina -= valor * Time.deltaTime;
        }
    }

    public void SubirEnergia()
    {
        if (Estamina < MaxEstamina ) 
        {
            Estamina += valor * Time.deltaTime;

        }
        
    }
}
