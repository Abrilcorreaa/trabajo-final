using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Correr : MonoBehaviour
{
    public float velocidad = 15;
    
    private estamina Estamina;

    void Start()
    {
        Estamina = GetComponent<estamina>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Estamina.Estamina > 0)
        {
            transform.position += transform.forward * Time.deltaTime * velocidad;
            Estamina.BajarEnergia(); 
        }
        else
        {
            Movimiento mov = FindObjectOfType<Movimiento>();
            Estamina.SubirEnergia(); 
        }
    }
}