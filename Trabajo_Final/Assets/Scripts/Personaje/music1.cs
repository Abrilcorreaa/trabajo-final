using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music1 : MonoBehaviour
{
    public Transform objeto1;
   
    public string nombreMusica = "musica_cercania";
    private AudioManager audioManager;

    void Start()
    {
       
        audioManager = AudioManager.instancia;
        audioManager.ReproducirSonido(nombreMusica);

    }

    void Update()
    {
        if (objeto1 != null)
        {
           

            float distancia = Vector3.Distance(transform.position, objeto1.position);
            float distanciaMaxima = 22f;

            float factorVolumen = Mathf.Clamp01(1 - (distancia / distanciaMaxima));
        
            audioManager.sonidos[0].FuenteAudio.volume = factorVolumen;
           
        }
        
    }
}
