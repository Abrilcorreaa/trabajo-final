using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music3 : MonoBehaviour
{
    
    public Transform objeto3;
    public string nombreMusica = "musica_cercania2";
    private AudioManager audioManager;

    void Start()
    {
       
        audioManager = AudioManager.instancia;
        audioManager.ReproducirSonido(nombreMusica);
    }

    void Update()
    {
        if ( objeto3 != null)
        {
            
            float distancia3 = Vector3.Distance(transform.position, objeto3.position);
            float distanciaMaxima = 22f;

            float factorVolumen3 = Mathf.Clamp01(1 - (distancia3 / distanciaMaxima));
            
            audioManager.sonidos[2].FuenteAudio.volume = factorVolumen3;


        }
        
    }
}
