using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
public class Recolectables : MonoBehaviour
{
    public int id;
    public string tipo;
    public Sprite imagen;
    public bool recolectado;
    public bool equipado;
    public GameObject partsManager;
    public bool pieza_jugador;
    public GameObject parts;


    private void Start()
    {
        partsManager = GameObject.FindWithTag("Parts_Manager");

        if (!pieza_jugador)
        {
            int Piezas = partsManager.transform.childCount;
            for (int i = 0; i < Piezas; i++)
            {
                if (partsManager.transform.GetChild(i).gameObject.GetComponent<Recolectables>().id == id)
                {
                    parts = partsManager.transform.GetChild(i).gameObject;
                    
                }

            }
        }
      
    }
    private void Update()
    {
        if (equipado)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                equipado = false;
            }

            if (equipado == false)
            {
                gameObject.SetActive(false);
            }

        }
    }

    public void utilidad()
    {
        if (tipo == "pieza" && parts != null)
        {
            parts.SetActive(true);
            Recolectables recolectablesComponent = parts.GetComponent<Recolectables>();
            if (recolectablesComponent != null)
            {
                recolectablesComponent.equipado = true;
            }
        }
    }
    public void EliminarDelInventario()
    {
        slot slotComponent = GetComponentInParent<slot>();

        if (slotComponent != null)
        {
            Destroy(slotComponent.gameObject);
        }

    }


}