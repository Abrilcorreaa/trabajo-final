using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class slot : MonoBehaviour, IPointerClickHandler
{
    public GameObject item;
    public int id;
    public string tipo;
    public bool vacio;
    public Sprite imagen;

    public Transform imagen_slot;
    public Image image;

    private void Start()
    {
        imagen_slot = transform.GetChild(0);
        image = GetComponent<Image>();
    }

    public void update_slot()
    {
        if (imagen_slot != null)
        {
            Image slotImage = imagen_slot.GetComponent<Image>();
            if (slotImage != null)
            {
                slotImage.sprite = imagen;
            }
        }

        if (image != null)
        {
            image.sprite = imagen;
        }
    }

    public void usar_item()
    {
        if (item != null)
        {
            item.GetComponent<Recolectables>().utilidad();
            LimpiarSlot(); 
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        usar_item();
    }

    public void LimpiarSlot()
    {
        if (item != null)
        {
            Destroy(item);
            if (imagen_slot != null)
            {
                
                imagen_slot.gameObject.SetActive(true);
            }
            item = null;
            id = 0;
            tipo = string.Empty;
            vacio = true;
            imagen = null;
            update_slot();
        }
    }
}