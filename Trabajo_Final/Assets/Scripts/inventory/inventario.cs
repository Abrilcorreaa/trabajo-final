using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventario : MonoBehaviour
{
    private bool Inventary_on_off;
    public GameObject inventary;
    private int all_slots;
    private int enable_slots;
    private GameObject[] slots;
    public GameObject slots_holder;


    void Start()
    {
        all_slots = slots_holder.transform.childCount;
        slots = new GameObject [all_slots];
        
        for (int i = 0; i < all_slots; i++)
        {
            slots[i] = slots_holder.transform.GetChild(i).gameObject;
           
            if (slots[i].GetComponent<slot>().item == null)
            {
                slots[i].GetComponent<slot>().vacio = true;
                
            }
            
            
            slots[i].GetComponent<slot>().update_slot();
        } 
        
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Inventary_on_off =!Inventary_on_off;
        }
        if (Inventary_on_off)
        {
            inventary.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            inventary.SetActive(false);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            GameObject item_recolectado=other.gameObject;
            Recolectables item = item_recolectado.GetComponent<Recolectables>();

            mostrar_item(item_recolectado,item.id,item.tipo,item.imagen);
        }
    }

    public void mostrar_item(GameObject item_object, int item_id, string item_tipo, Sprite item_imagen)
    {
        for (int i = 0; i < all_slots; i++)
        {
            if (slots[i].GetComponent<slot>().vacio)
            {
                item_object.GetComponent<Recolectables>().recolectado = true;

                slots[i].GetComponent<slot>().item = item_object;
                slots[i].GetComponent<slot>().id = item_id;
                slots[i].GetComponent<slot>().tipo = item_tipo;

                slots[i].GetComponent<slot>().imagen = item_imagen;

                item_object.transform.parent = slots[i].transform;
                item_object.SetActive(false);

                
                slots[i].GetComponent<slot>().update_slot();

                slots[i].GetComponent<slot>().vacio = false;

                return;
            }
        }
    }

}
