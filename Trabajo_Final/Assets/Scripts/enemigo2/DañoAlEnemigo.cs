using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoAlEnemigo : MonoBehaviour
{
    public int daño = 1;

    dropEnemigo items;
    void Start()
    {
        items = GetComponent<dropEnemigo>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Vida vidaEn2 = GetComponent<Vida>();

        if (other.CompareTag("Bala"))
        {
            vidaEn2.vida -= daño;

        }
        if (vidaEn2.vida == 0)
        {
            Destroy(gameObject);
            items.itemDrop();

        }

    }

}
