using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerseguirJugador : MonoBehaviour
{
    public float Rango;
    public LayerMask CapaJugador;

    bool alerta;
    public Transform jugador;
    public float velocidad;

    void Update()
    {
        alerta = Physics.CheckSphere(transform.position, Rango, CapaJugador);
        if (alerta)
        {
            Vector3 Posicion_jug = new Vector3(jugador.position.x, transform.position.y, jugador.position.z);
            transform.LookAt(Posicion_jug);
            transform.position = Vector3.MoveTowards(transform.position, Posicion_jug, velocidad * Time.deltaTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, Rango);
    }

    
    public bool EstŠEnAlerta()
    {
        return alerta;
    }
}
