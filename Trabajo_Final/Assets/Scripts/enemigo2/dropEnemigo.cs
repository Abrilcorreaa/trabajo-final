using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropEnemigo : MonoBehaviour
{
    public GameObject[] items;
    int activarItem;

    void Start()
    {
        activarItem = Random.Range(0,items.Length);
    }

    public void itemDrop()
    {
        Instantiate(items[activarItem],transform.position, Quaternion.identity);
    }



}
