using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public float velocidad = 5;
    Vector3 direccion = Vector3.right;

    public PerseguirJugador perseguidor;

    void Update()
    {
        MoverContinuamente();
    }

    void MoverContinuamente()
    {
        
        if (perseguidor != null && perseguidor.EstŠEnAlerta())
        {
            
            direccion = Vector3.zero;
        }
        else
        {
            
            if (transform.position.x >= -130.3763f)
            {
                CambiarDireccion(Vector3.left);
            }
            else if (transform.position.x <= -135.3863f)
            {
                CambiarDireccion(Vector3.right);
            }
        }

        transform.position += direccion * Time.deltaTime * velocidad;
    }

    void CambiarDireccion(Vector3 nuevaDireccion)
    {
        direccion = nuevaDireccion;
    }
}