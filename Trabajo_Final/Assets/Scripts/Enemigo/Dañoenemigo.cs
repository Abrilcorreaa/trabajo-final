using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dañoenemigo : MonoBehaviour
{
    public float Rango;
    public LayerMask CapaJugador;

    public GameObject BolaDeFuego;
    public Transform UbiBolaDeFuego;
    public float VelBolaDeFuego = 1f;
    private Transform jugador;
    bool alerta;

    void Start()
    {
        jugador = FindObjectOfType<Movimiento>().transform;

    }

    void Update()
    {
        alerta = Physics.CheckSphere(transform.position, Rango, CapaJugador);
        if (alerta == true)
        {
            Invoke("Disparo", 1);

        }

    }


    void Disparo()
    {
        Vector3 direccion_jugador = (jugador.position - transform.position).normalized;
        transform.LookAt(jugador.position);

        GameObject Bala2;
        Bala2 = Instantiate(BolaDeFuego, UbiBolaDeFuego.position, UbiBolaDeFuego.rotation);

        Bala2.GetComponent<Rigidbody>().AddForce(direccion_jugador * VelBolaDeFuego, ForceMode.Impulse);
        CancelInvoke("Disparo");
    }



    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, Rango);
    }
}
