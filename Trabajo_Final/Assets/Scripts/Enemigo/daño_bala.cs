using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class daño_bala : MonoBehaviour
{
    public int daño = 20;
    private void OnTriggerEnter(Collider other)
    {
        EnemigoVida vidaEn = GetComponent<EnemigoVida>();

        if (other.CompareTag("Bala"))
        {
            vidaEn.vida -= daño;

        }
        if (vidaEn.vida == 0)
        {
            Destroy(gameObject);

        }

    }

}
