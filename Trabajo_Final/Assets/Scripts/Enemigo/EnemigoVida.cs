using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemigoVida : MonoBehaviour
{
    public int vida;
    public Slider barraVida;

    private void Update()
    {
        barraVida.value = vida;

    }
   
}
