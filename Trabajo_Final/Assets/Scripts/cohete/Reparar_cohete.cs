using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reparar_cohete : MonoBehaviour
{
    public int piezasNecesarias = 3;
    public int piezasRecolectadas = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            Recolectables pieza = other.GetComponent<Recolectables>();

            pieza.EliminarDelInventario();
           
            piezasRecolectadas ++;
            pieza.recolectado = true;

            other.gameObject.SetActive(false);

            pieza.EliminarDelInventario();

            GameManager gameManager = FindObjectOfType<GameManager>();

            gameManager.addnum();

            if (piezasRecolectadas == piezasNecesarias)
            {
                
                Reparar();
               
            }
            
        }
    }
   
    void Reparar()
    {

        SceneManager.LoadScene("Final");
        AudioManager.instancia.ReproducirSonido("musica_final");

    }
 

}
