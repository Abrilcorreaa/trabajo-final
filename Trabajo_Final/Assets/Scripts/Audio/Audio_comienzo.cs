using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class Audio_comienzo : MonoBehaviour
{
    private AudioSource source;
    public static Audio_comienzo instance;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        source = gameObject.AddComponent<AudioSource>();
    }
    public void Say(AudioObject clip)
    {
        if(source.isPlaying)
        {
            source.Stop();
        }
        source.PlayOneShot(clip.clip);

        Subtitulos.instance.PonerSubtitulos(clip.subtitle,clip.clip.length);
       
    }
}
