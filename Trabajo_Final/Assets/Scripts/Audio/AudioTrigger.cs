using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    public AudioObject ClipToPlay;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")){
            Audio_comienzo.instance.Say(ClipToPlay);

        }
    }
}
