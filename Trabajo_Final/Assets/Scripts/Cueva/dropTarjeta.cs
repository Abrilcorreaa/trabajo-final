using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropTarjeta : MonoBehaviour
{
    Rigidbody rb;
    public float saltoTarjeta;


    void Start()
    {
        rb = GetComponent<Rigidbody>();

        rb.velocity = new Vector3(saltoTarjeta * Time.deltaTime, rb.velocity.y, saltoTarjeta * Time.deltaTime);
    }

   
}
