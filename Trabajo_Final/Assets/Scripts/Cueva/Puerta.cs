using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public string nombreAnimacion;
    public Animator animator;
    public string nombreAnimacion2;
    public Animator animator2;
    public string nombreAnimacion3;
    public Animator animator3;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.Play(nombreAnimacion);
            animator2.Play(nombreAnimacion2);
            animator3.Play(nombreAnimacion3);
            Destroy(gameObject);
        }
    }
}
