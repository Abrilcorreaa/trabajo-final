using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tarjeta : MonoBehaviour
{
    public GameObject TarjetaObjeto;
    public GameObject Puerta;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Puerta.gameObject.SetActive(true);
            Destroy(TarjetaObjeto);
        }

    }
    

}
